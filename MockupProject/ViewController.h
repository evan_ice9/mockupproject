
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UILabel *quizLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentBtn;
- (IBAction)segmentBtnClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *correctLabel;
@property (strong, nonatomic) IBOutlet UILabel *incorrectLabel;

@end


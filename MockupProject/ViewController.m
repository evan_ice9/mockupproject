
#import "ViewController.h"
#import "NetworkController.h"

@interface ViewController ()

@end


int currentQuizOffset,correctCount,incorrectCount;
NetworkController *net;
NSArray  *quizNames;
NSArray  *quizResults;

@implementation ViewController

@synthesize segmentBtn,quizLabel,spinner,correctLabel,incorrectLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    currentQuizOffset = 0;
    correctCount = 0;
    incorrectCount = 0;
    
    net = [NetworkController sharedInstance];
    [spinner startAnimating];
    
    [net GET:@"quiz.json" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        [spinner stopAnimating];
        quizNames = responseObject[@"elements"];
        quizResults = responseObject[@"result"];
        segmentBtn.hidden = false;
        quizLabel.hidden = false;
        
        [self setupViewWithQuizAtIndex:currentQuizOffset];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
       [spinner stopAnimating];
        NSLog(@"%@",error.description);
    }];
    
}


-(void)loadCorrectCounts
{
    correctLabel.text = [NSString stringWithFormat:@"correct : %d",correctCount];
    incorrectLabel.text = [NSString stringWithFormat:@"incorrect : %d",incorrectCount];
}
-(void)setupViewWithQuizAtIndex:(int)offset
{
    NSString *quizTitle = quizNames[offset];
    quizLabel.text = quizTitle;
    [self loadCorrectCounts];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)segmentBtnClicked:(id)sender
{
    
    switch (segmentBtn.selectedSegmentIndex)
    {
        case 0:
        {
            NSLog(@"fly");
            [self showAlertViewWithAnswer:true];
            
        }
            break;
        case 1:
        {
            NSLog(@"Don't fly");
            [self showAlertViewWithAnswer:false];
        }
            break;
            
        default:
            break;
    }
    
}



-(void)showAlertViewWithAnswer:(BOOL)ans
{
    NSString *alertMsg;
    BOOL res = [quizResults[currentQuizOffset] boolValue];
    
    if(ans == res)
    {
        alertMsg = @"correct";
    }
    else
    {
        alertMsg = @"incorrect";
    }
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Result ?" message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *next = [UIAlertAction
                         actionWithTitle:@"next"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             currentQuizOffset++;                             
                             if(ans == res)
                             {
                                 correctCount++;
                             }
                             else
                             {
                                 incorrectCount++;
                             }
                             
                             [self setupViewWithQuizAtIndex:currentQuizOffset];
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction  *close = [UIAlertAction
                             actionWithTitle:@"close"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                                 if(currentQuizOffset == quizNames.count - 1)
                                 {
                                     if(correctCount + incorrectCount <  quizNames.count)
                                     {
                                         if(ans == res)
                                         {
                                             correctCount++;
                                         }
                                         else
                                         {
                                             incorrectCount++;
                                         }
                                         
                                         [self loadCorrectCounts];
                                     }
                                     
                                     
                                 }
                             }];
    
    if(currentQuizOffset < quizNames.count - 1)
    {
        [alert addAction:next];
    }
    
    [alert addAction:close];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    


}

@end

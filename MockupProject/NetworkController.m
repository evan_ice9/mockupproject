
#define baseUrl @"http://www.siliconorchard.com/app/"

#import "NetworkController.h"


@implementation NetworkController


+(id)sharedInstance
{
    static NetworkController *instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,
    ^{
        instance = [[NetworkController alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
        instance.responseSerializer = [AFJSONResponseSerializer serializer];
    });
    
    
    return instance;
}



@end


#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface NetworkController : AFHTTPSessionManager
+(id)sharedInstance;
@end
